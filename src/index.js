
function PasswordValidator(data) {

  /**
   * default data 
   */
  const options = {
    value: '',
    min: 6,
    max: 15,
    isMin: false,
    isMax: false,
    isUpperCase: false,
    isLowerCase: false,
    isSpecialChar: false,
    isSpaceCheck: false,
    isNumeric: false,
  }

  /**
   * init results data
   */
  const result = {
    isMin: false,
    isMax: false,
    isContainUpperCase: false,
    isContainLowerCase: false,
    isContainSpecialChar: false,
    isContainSpace: false,
    isContainNumeric: false,
    isValid: false,
  }

  /**
   * special char regEx
   */
  const specialCharRegEx = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

  /**
   * space check regEX
   */
  const spaceRegEx = /\s/g;

  /**
   * props value to default options
   */
  options.value = data.value ? data.value : options.value;
  options.min = data.min ? data.min : options.min;
  options.max = data.max ? data.max : options.max;


  /**
   * check password min length validation
   */
  if (options.value.length >= options.min) {
    result.isMin = true;
  } else {
    result.isMin = false;
  }

  /**
   * check password max length validation
   */
  if (options.value.length > options.min && options.value.length <= options.max) {
    result.isMax = true;
  } else {
    result.isMax = false;
  }

  /**
   * check password contains upper case char
   */
  result.isContainUpperCase = options.value.match("[A-Z]") ? true : false;


  /**
   * check password contains lower case char
   */
  result.isContainLowerCase = options.value.match("[a-z]") ? true : false;

  /**
   * check password contains numeric char
   */
  result.isContainNumeric = options.value.match("[0-9]") ? true : false;

  /**
   * check password contain special char
   */
  result.isContainSpecialChar = specialCharRegEx.test(options.value) ? true : false;

  /**
   * check password contain space
   */
  result.isContainSpace = spaceRegEx.test(options.value) ? true : false;

  if (result.isMin && result.isMax && result.isContainNumeric && result.isContainSpecialChar && result.isContainUpperCase && result.isContainLowerCase) {
    result.isValid = true;
  }

  return result;
}

export default PasswordValidator;