import React, { useState } from 'react'

import PasswordValidator from 'password-validator'
import 'password-validator/dist/index.css';

function App() {
  const [result, setResults] = useState(null);
  const [password, setInputs] = useState('');


  function OnChange  (e) {

    const { value } = e.target;
    setInputs(value);
    
    const post = {
      min: 6,
      max: 10,
      upperCase: true,
      lowerCase: true,
      value: password,
      isSpecialCheck: true,
      isSpaceCheck: true
    }
    const res = PasswordValidator(post);
    setResults(res);

  }

  function onSubmit(e) {
    e.preventDefault();
  }
  return (
    <>
      <form onSubmit={onSubmit} className="text-center" autoComplete="off" >
        <div className="mt-5">
          <h3 className="text-center">Password Validator</h3>
        </div>
        <div className="form-group col-12 mt-5">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input type="text" className="form-control" name="password" id="exampleInputPassword1" value={password} placeholder="Password" onChange={OnChange} />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
      {result && <div className="text-center">
        <div>isMin : {result.isMin ? 'True' : 'False'}</div>
        <div>isMax : {result.isMax ? 'True' : 'False'}</div>
        <div>isContainUpperCase : {result.isContainUpperCase ? 'True' : 'False'}</div>
        <div>isContainLowerCase : {result.isContainLowerCase ? 'True' : 'False'}</div>
        <div>isContainSpecialChar : {result.isContainSpecialChar ? 'True' : 'False'}</div>
        <div>isContainSpace : {result.isContainSpace ? 'True' : 'False'}</div>
        <div>isContainNumeric : {result.isContainNumeric ? 'True' : 'False'}</div>
        <div>isValid : {result.isValid ? 'True' : 'False'}</div>
      </div>}
    </>
  )
}

export default App